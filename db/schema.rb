# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120526204628) do

  create_table "broadcasts", :force => true do |t|
    t.string   "song_name"
    t.string   "interpret"
    t.text     "raw"
    t.integer  "radio_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "song_id"
  end

  add_index "broadcasts", ["created_at"], :name => "index_broadcasts_on_created_at"
  add_index "broadcasts", ["radio_id"], :name => "index_broadcasts_on_radio_id"
  add_index "broadcasts", ["song_id"], :name => "index_broadcasts_on_song_id"

  create_table "interprets", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "radios", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "fancyname"
  end

  create_table "search_engines", :force => true do |t|
    t.string   "name"
    t.string   "query_string"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "songs", :force => true do |t|
    t.string   "name",         :null => false
    t.integer  "interpret_id"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "songs", ["id"], :name => "index_songs_on_id"

  create_table "transmitters", :force => true do |t|
    t.string   "name"
    t.integer  "radio_id"
    t.float    "fq"
    t.integer  "range"
    t.float    "lat"
    t.float    "long"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
