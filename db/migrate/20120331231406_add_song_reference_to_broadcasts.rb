class AddSongReferenceToBroadcasts < ActiveRecord::Migration
  def change
	add_column :broadcasts, :song_id, :integer, :references=>"songs"
  end
end
