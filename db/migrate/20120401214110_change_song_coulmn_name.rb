class ChangeSongCoulmnName < ActiveRecord::Migration
  def up
    change_table :broadcasts do |t|
      t.rename :song, :song_name
    end
  end

  def down
    change_table :broadcasts do |t|
      t.rename :song_name, :song
    end
  end
end
