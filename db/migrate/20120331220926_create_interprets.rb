class CreateInterprets < ActiveRecord::Migration
  def change
    create_table :interprets do |t|
		t.string :interpret

      t.timestamps
    end
  end
end
