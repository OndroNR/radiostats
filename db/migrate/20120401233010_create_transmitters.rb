class CreateTransmitters < ActiveRecord::Migration
  def up
    create_table :transmitters do |t|
		t.string :name
		t.references :radio
		t.float :fq
		t.integer :range
		t.float :lat
		t.float :long
      t.timestamps
    end
  end
  
  def down
	drop_table :transmitters
  end
end
