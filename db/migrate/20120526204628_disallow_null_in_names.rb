class DisallowNullInNames < ActiveRecord::Migration
  def up
	change_column :songs, :name, :string, :null => false
	change_column :interprets, :name, :string, :null => false
  end

  def down
	change_column :songs, :name, :string, :null => true
	change_column :interprets, :name, :string, :null => true
  end
end
