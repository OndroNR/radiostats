class AddIndexBroadcastsCreatedAt < ActiveRecord::Migration
  def change
	add_index :broadcasts, :created_at
  end
end
