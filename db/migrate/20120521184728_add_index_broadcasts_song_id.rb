class AddIndexBroadcastsSongId < ActiveRecord::Migration
  def change
	add_index :broadcasts, :song_id
  end
end
