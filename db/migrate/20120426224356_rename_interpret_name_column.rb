class RenameInterpretNameColumn < ActiveRecord::Migration
  def up
	change_table :interprets do |t|
		t.rename :interpret, :name
	end
  end

  def down
	change_table :interprets do |t|
		t.rename :name, :interpret
	end
  end
end
