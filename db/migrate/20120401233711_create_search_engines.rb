class CreateSearchEngines < ActiveRecord::Migration
  def up
    create_table :search_engines do |t|
		t.string :name
		t.string :query_string
      t.timestamps
    end

  end

  def down
	drop_table :search_engines
  end
end
