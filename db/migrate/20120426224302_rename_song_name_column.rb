class RenameSongNameColumn < ActiveRecord::Migration
  def up
	change_table :songs do |t|
		t.rename :song, :name
	end
  end

  def down
	change_table :songs do |t|
		t.rename :name, :song
	end
  end
end