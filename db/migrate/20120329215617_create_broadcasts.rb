class CreateBroadcasts < ActiveRecord::Migration
  def change
    create_table :broadcasts do |t|
      t.string :song
      t.string :interpret
      t.text :raw
      t.references :radio

      t.timestamps
    end
    add_index :broadcasts, :radio_id
  end
end
