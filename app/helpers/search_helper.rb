# encoding: utf-8

module SearchHelper
	def week_value
		cookies[:week].to_i
	end

	def week_values
		oldest = Broadcast.select('WEEK(created_at) as week').order('created_at').limit(1).first.week
		newest = Broadcast.select('WEEK(created_at) as week').order('created_at DESC').limit(1).first.week
		
		weeks = (oldest..newest).map do |w| [w, w] end
		weeks.insert 0, ["Všetky", 0]
	end
end
