class Broadcast < ActiveRecord::Base
  belongs_to :radio
  belongs_to :song
  
  def save_if_different_than_latest

		last_song = Broadcast.where(:radio_id => self.radio_id).order("created_at DESC").limit(1)
		  
		if last_song.count > 0
			if last_song.first.song_id == self.song_id
				return
			end
		end

		self.save!
  end
  
  def fill_song_id
	  i = Interpret.get_by_name(self.interpret)
	  s = Song.get_by_name_and_interpret(self.song_name, i)
	  self.song_id = s.id;
  end
  
  class << self
	  def week_filter(week)
		if week.to_i > 0
			where('WEEK(broadcasts.created_at) = :w', w: week.to_i)
		else
			self
		end
	  end
  end
end
