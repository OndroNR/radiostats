class Radio < ActiveRecord::Base
	has_many :broadcasts
	
	def songs_last(last=20)
		Broadcast.where(:radio_id => self.id).order("created_at DESC").includes(:song => [:interpret]).limit(last)
	end
end
