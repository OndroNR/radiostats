class Interpret < ActiveRecord::Base
	has_many :songs
	
	def self.get_by_name(name)
		i = Interpret.new
		
		#i.interpret = name.strip.to_ascii
		i.name = name.strip
		
		result = Interpret.find_by_name(i.name)
		
		i.save unless result
		
		(result) ? result : i
	end
end
