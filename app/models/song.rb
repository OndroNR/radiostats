class Song < ActiveRecord::Base
	belongs_to :interpret
	has_many :broadcast

	def self.get_by_name_and_interpret(name, interpret)
		s = Song.new
		
		s.name = name.strip
		s.interpret_id = interpret.id
		
		result = Song.find_by_name_and_interpret_id(s.name, interpret.id)
		
		s.save unless result
		
		(result) ? result : s
	end	

  class << self
	  def week_filter(week)
		if week.to_i > 0
			where('WEEK(broadcasts.created_at) = :w', w: week.to_i)
		else
			self
		end
	  end
  end
end
