require 'open-uri'
require 'net/http'

class CronController < ApplicationController
	def funradio
		br = Radiostats::Parsers::SepiaParser::parse(open('http://funradio.sk/sepia/sepia_data_live.xml'), Broadcast.new)
		br.radio_id = 1
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # funradio.html.erb
		  format.json { render json: br }
		end
	end
	
	def radiomax
		br = Radiostats::Parsers::RadiomaxParser::parse(open('http://www.radiomax.sk/nowplay.php'), Broadcast.new)
		br.radio_id = 2
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)

		respond_to do |format|
		  format.html # radiomax.html.erb
		  format.json { render json: br }
		end
	end
	
	def expres
		br = Radiostats::Parsers::ExpresParser::parse(open('http://www.expres.sk/'), Broadcast.new)
		br.radio_id = 3
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)

		respond_to do |format|
		  format.html # expres.html.erb
		  format.json { render json: br }
		end
	end

	def europa2
		br = Radiostats::Parsers::Europa2Parser::parse(open('http://www.europa2.sk/sk/system/_rds-actual.js'), Broadcast.new)
		br.radio_id = 4
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # europa2.html.erb
		  format.json { render json: br }
		end
	end

	def viva
		url = URI.parse('http://www.radioviva.sk/load_skladbu.php')
		post_args = { 'playlist' => 1 }
		resp = Net::HTTP::post_form(url, post_args)

		br = Radiostats::Parsers::VivaParser::parse(resp.body, Broadcast.new)
		br.radio_id = 5
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # viva.html.erb
		  format.json { render json: br }
		end
	end

	def hey
		url = 'http://www.radiohey.sk/!utils/playlist/playlist.dat?d=' + Time.now.strftime('%-m-%-d-%H-%M-%S')
		br = Radiostats::Parsers::HeyParser::parse(open(url).read, Broadcast.new)
		br.radio_id = 6
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # viva.html.erb
		  format.json { render json: br }
		end
	end

	def antena
		url = 'http://www.antenahitradio.sk/templates/antena_sub/includes/get_song.php?_=' + Time.now.strftime('%-m-%-d-%H-%M-%S')
		br = Radiostats::Parsers::AntenaParser::parse(open(url).read, Broadcast.new)
		br.radio_id = 7
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # viva.html.erb
		  format.json { render json: br }
		end
	end
	
	def jemnemelodie
		url = 'http://www.jemnemelodie.sk/cms/data.php?act=' + Time.now.strftime('%-m-%-d-%H-%M-%S')
		br = Radiostats::Parsers::JemnemelodieParser::parse(open(url).read, Broadcast.new)
		br.radio_id = 8
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # viva.html.erb
		  format.json { render json: br }
		end
	
	end

	def jemnemelodie
		url = 'http://www.jemnemelodie.sk/cms/data.php?act=' + Time.now.strftime('%-m-%-d-%H-%M-%S')
		br = Radiostats::Parsers::JemnemelodieParser::parse(open(url).read, Broadcast.new)
		br.radio_id = 8
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		respond_to do |format|
		  format.html # viva.html.erb
		  format.json { render json: br }
		end
	end
	
	def radio_slovensko
		rozhlas 'http://rozhlas.sk/radio-slovensko', 9
	end
	
	def radio_klasika
		rozhlas 'http://rozhlas.sk/radio-klasika', 11
	end
	
	def radio_regina
		rozhlas 'http://rozhlas.sk/radio-regina', 12
	end
	
	def radio_patria
		rozhlas 'http://rozhlas.sk/radio-patria', 13
	end
	
	def radio_litera
		rozhlas 'http://rozhlas.sk/radio-litera', 14
	end
	
	def radio_devin
		rozhlas 'http://rozhlas.sk/radio-devin', 15
	end

	def radio_slovakia_international
		rozhlas 'http://rozhlas.sk/radio-international-sk', 16
	end

	def radio_junior
		rozhlas 'http://rozhlas.sk/radio-junior', 17
	end
	
	def rozhlas(url, radio_id)
		br = Radiostats::Parsers::RozhlasParser::parse(open(url).read, Broadcast.new)
		br.radio_id = radio_id
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		@radio = Radio.find radio_id
		
		respond_to do |format|
		  format.html { render :template => 'cron/rozhlas' }# viva.html.erb
		  format.json { render json: br }
		end
	end
	
	def radio_fm
		br = Radiostats::Parsers::RadioFmParser::parse(open('http://www.radiofm.sk/sepia/lastsong_fm.xml'), Broadcast.new)
		br.radio_id = 10
		br.fill_song_id
		br.save_if_different_than_latest
		brToTemplate(br)
		
		@radio = Radio.find br.radio_id
		
		respond_to do |format|
		  format.html { render :template => 'cron/rozhlas' }
		  format.json { render json: br }
		end
	end

	
	def brToTemplate(br)
		@songname = br.song_name
		@songinterpret = br.interpret
	end
end
