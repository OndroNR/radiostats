class SearchController < ApplicationController
  def search
	if !params[:q].empty?
		@interprets = Interpret.where('name like :query', query: "%#{params[:q]}%")
		@songs = Song.where('name like :query', query: "%#{params[:q]}%")
	end
	
	if !params[:time].empty?
		time = (Broadcast.sanitize(params[:time] + ' +0200')).to_datetime
		time_span = (time - 5.minutes)..(time + 5.minutes)
		@broadcasts = Broadcast.where(:created_at=>time_span).order("created_at DESC").includes(:song => [:interpret], :radio => [])
	end
  end
  
  def filter
	cookies[:week] = params[:week].to_i
	redirect_to params[:return]
  end
end