class InterpretsController < ApplicationController
  def index
	#@interprets = Interpret.paginate(:per_page => 30, :page => params[:page], :order => 'name')
	@interprets = Interpret.order(:name).page params[:page]
  end

  # GET /radios/1
  # GET /radios/1.json
  def show
	@interpret = Interpret.find(params[:id])

	@last_played = Broadcast.joins(:song).where(:songs => {:interpret_id => params[:id]}).order("broadcasts.created_at DESC").includes(:song, :radio).limit(10)
	
	
	@total_played = Broadcast.week_filter(cookies[:week]).joins(:song).where(:songs => {:interpret_id => params[:id]}).count()

	@total_played_on_radios = Broadcast.week_filter(cookies[:week]).select('radio_id, COUNT(*) as cnt').joins(:song).where(:songs => {:interpret_id => params[:id]}).group('radio_id').includes(:radio)

	@all_songs = Song.week_filter(cookies[:week]).select('songs.id, songs.name, COUNT(*) as cnt').joins(:broadcast).where(:interpret_id => params[:id]).group('songs.id')
  end
end
