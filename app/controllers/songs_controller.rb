require 'cgi'

class SongsController < ApplicationController
  def index
	@last_played = Broadcast.order("created_at DESC").includes(:song => [:interpret], :radio => []).limit(20)

	@most_played = Broadcast.select("*, COUNT(song_id) AS cnt").group("song_id").order("cnt DESC").includes(:song => [:interpret], :radio => []).limit(20)
  end
  
  def most_played
	@week = cookies[:week].to_i
	@week = Broadcast.select('WEEK(created_at) as week').order('created_at DESC').limit(1).first.week if @week == 0
	
	
	@most_played = Broadcast.select("*, COUNT(song_id) AS cnt").week_filter(@week).group("song_id").order("cnt DESC").includes(:song => [:interpret]).limit(20)  
  end

  # GET /radios/1
  # GET /radios/1.json
  def show
	@song = Song.find(params[:id])

	@last_played = Broadcast.where(:song_id => params[:id]).order("created_at DESC").limit(10)

	@total_played = Broadcast.week_filter(cookies[:week]).where(:song_id => params[:id]).count("id")
	
	@total_played_on_radios = Broadcast.week_filter(cookies[:week]).select("radio_id, COUNT(radio_id) AS cnt").where(:song_id => params[:id]).group("radio_id")
	
	@youtube_url = "http://www.youtube.com/results?search_query=" + CGI.escape(@song.name + " " + @song.interpret.name)
	
	@grooveshark_url = "http://grooveshark.com/#!/search?q=" + CGI.escape(@song.name + " " + @song.interpret.name)
	
	#@week_graph = Broadcast
	#					.select('YEAR(created_at) as year, WEEK(created_at) as w, COUNT(*) as c')
	#					.where(:song_id => @song.id)
	#					.group('YEAR(created_at), WEEK(created_at)')
	#					.map do |br|
	#						#[br.year.to_s + " " + br.w.to_s, br.c]
	#						[br.w, br.c]
	#					end
	
	@week_radio = Array.new
	Radio.all.each do |radio|
		r = Broadcast
						.select('YEAR(created_at) as year, WEEK(created_at) as w, COUNT(*) as c')
						.where(:song_id => @song.id, :radio_id => radio.id)
						.group('YEAR(created_at), WEEK(created_at)')
						.map do |br|
							#[br.year.to_s + " " + br.w.to_s, br.c]
							[br.w, br.c]
						end
		@week_radio.push({ :data => r, :label => radio.name }) unless r.empty?
	end
  end
end
