class HomeController < ApplicationController
  def index
	@last_played = Broadcast.order("created_at DESC").includes(:song => [:interpret], :radio => []).limit(10)

	last_week = 7.days.ago..Time.now
	
	@most_played = Broadcast.select("*, COUNT(song_id) AS cnt").where(:created_at => last_week).group("song_id").order("cnt DESC").includes(:song => [:interpret], :radio => []).limit(10)
  end
  
  def about
  end
end
