class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :prepare_layout
  before_filter :miniprofiler
  
  def prepare_layout
	@all_radios = Radio.all.order(:name)
  end

  def miniprofiler
    if params[:profiler] == "yes"
      Rack::MiniProfiler.authorize_request
    end
  end
end
