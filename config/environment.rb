# Load the rails application
require File.expand_path('../application', __FILE__)

require File.expand_path('../../lib/radiostats/tools.rb', __FILE__)

# Initialize the rails application
Radiostats::Application.initialize!
