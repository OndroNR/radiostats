Radiostats::Application.routes.draw do
  resources :radios

  get "home/index"
  
  get 'about' => 'home#about'
  
  get 'cron/funradio' => 'cron#funradio'
  get 'cron/radiomax' => 'cron#radiomax'
  get 'cron/expres' => 'cron#expres'
  get 'cron/europa2' => 'cron#europa2'
  get 'cron/viva' => 'cron#viva'
  get 'cron/hey' => 'cron#hey'
  get 'cron/antena' => 'cron#antena'
  get 'cron/jemnemelodie' => 'cron#jemnemelodie'
  get 'cron/radio_slovensko' => 'cron#radio_slovensko'
  get 'cron/radio_klasika' => 'cron#radio_klasika'
  get 'cron/radio_regina' => 'cron#radio_regina'
  get 'cron/radio_patria' => 'cron#radio_patria'
  get 'cron/radio_litera' => 'cron#radio_litera'
  get 'cron/radio_devin' => 'cron#radio_devin'
  get 'cron/radio_slovakia_international' => 'cron#radio_slovakia_international'
  get 'cron/radio_junior' => 'cron#radio_junior'
  get 'cron/radio_fm' => 'cron#radio_fm'
  
  get 'song/:id' => 'songs#show', :as => 'song_link', :via => 'get'
  get 'songs' => 'songs#index'
  get 'most_played' => 'songs#most_played'

  resources :interprets do
    get 'page/:page', :action => :index, :on => :collection
  end
  get 'interpret/:id' => 'interprets#show', :as => 'interpret_link', :via => 'get'
  get 'interprets' => 'interprets#index'
  
  get 'search' => 'search#search'
  get 'filter' => 'search#filter'

  
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  root :to => 'home#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
