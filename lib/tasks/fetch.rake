require 'open-uri'
require 'nokogiri'

namespace :fetch do
	desc 'Fetch funradio current song'
	task :funradio => :environment do
		br = Radiostats::Parsers::SepiaParser::parse(open('http://funradio.sk/sepia/sepia_data_live.xml'), Broadcast.new)
		br.radio_id = 1
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'Funradio'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end

	
	desc 'Fetch radiomax current song'
	task :radiomax => :environment do
		br = Radiostats::Parsers::RadiomaxParser::parse(open('http://www.radiomax.sk/nowplay.php'), Broadcast.new)
		br.radio_id = 2
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'Radio.max'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end
	
	desc 'Fetch expres current song'
	task :expres => :environment do
		br = Radiostats::Parsers::ExpresParser::parse(open('http://www.expres.sk/'), Broadcast.new)
		br.radio_id = 3
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'Expres'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end

	desc 'Fetch europa2 current song'
	task :europa2 => :environment do
		br = Radiostats::Parsers::Europa2Parser::parse(open('http://www.europa2.sk/sk/system/_rds-actual.js'), Broadcast.new)
		br.radio_id = 4
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'Europa 2'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end

	desc 'Fetch viva current song'
	task :viva => :environment do
		url = URI.parse('http://www.radioviva.sk/load_skladbu.php')
		post_args = { 'playlist' => 1 }
		resp = Net::HTTP::post_form(url, post_args)

		br = Radiostats::Parsers::VivaParser::parse(resp.body, Broadcast.new)
		br.radio_id = 5
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'VIVA'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end	

	desc 'Fetch hey current song'
	task :hey => :environment do
		url = 'http://www.radiohey.sk/!utils/playlist/playlist.dat?d=' + Time.now.strftime('%-m-%-d-%H-%M-%S')
		br = Radiostats::Parsers::HeyParser::parse(open(url).read, Broadcast.new)
		br.radio_id = 6
		br.fill_song_id
		br.save_if_different_than_latest
		
		puts 'Radio HEY!'
		puts 'Interpret: ' + br.interpret
		puts 'Song: ' + br.song.song
	end	

	

	end