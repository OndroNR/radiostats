module Radiostats
	module Parsers
		class RozhlasParser
			def self.parse(html, instance)
				doc = Nokogiri::HTML(html)
				
				raw = doc.css('#justPlayingHolder').first.text.strip

				data = raw.split(':', 2).second
				
				parts = data.split(' - ', 2)
				
				if parts.count != 2
					raise "rozhlas parsing failed: count not 2"
				end
				
				instance.song_name = parts[1].strip.force_encoding('UTF-8').translit.upcase_word
				instance.interpret = parts[0].strip.force_encoding('UTF-8').translit.upcase_word

				instance.raw = raw
				
				instance
			end
		end
	end
end