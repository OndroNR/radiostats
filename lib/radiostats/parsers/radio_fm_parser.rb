require 'nokogiri'

module Radiostats
	module Parsers
		class RadioFmParser
			def self.parse(xml, instance)
				doc = Nokogiri::XML(xml)
				
				now = doc.css('track').first
				
				instance.song_name = now.css('song').first.content.strip.to_ascii.upcase_word
				instance.interpret = now.css('artist').first.content.strip.to_ascii.upcase_word
				instance.raw = now.to_xml
				
				instance
			end
		end
	end
end