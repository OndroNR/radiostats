require 'nokogiri'

module Radiostats
	module Parsers
		class ExpresParser
			def self.parse(html, instance)
				doc = Nokogiri::HTML(html)

				instance.song_name = doc.css("#song-second").first.text.strip.to_ascii.upcase_word
				instance.interpret = doc.css("#song-first").first.text.strip.to_ascii.upcase_word
				
				if instance.song_name == "Radio Expres"
					raise "No song info"
				end
				
				instance
			end
		end
	end
end