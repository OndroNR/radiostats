module Radiostats
	module Parsers
		class AntenaParser
			def self.parse(raw, instance)
				data = raw.split("***").first
				
				parts = data.split(' - ', 2)
				
				if parts.count != 2
					raise "antena parsing failed: count not 2"
				end
				
				instance.song_name = parts[1].strip.force_encoding('UTF-8').translit.upcase_word
				instance.interpret = parts[0].strip.force_encoding('UTF-8').translit.upcase_word

				instance.raw = raw
				
				instance
			end
		end
	end
end