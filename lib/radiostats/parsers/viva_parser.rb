# encoding: utf-8

module Radiostats
	module Parsers
		class VivaParser
			def self.parse(data, instance)
				parts = data.split(";;;")
												
				instance.song_name = parts[1].strip.force_encoding('UTF-8').translit.upcase_word
				instance.interpret = parts[4].strip.force_encoding('UTF-8').translit.upcase_word
				instance.raw = data
				
				if instance.song_name == "*** Radio Viva ***"
					raise 'No song info'
				end
				
				instance
			end
		end
	end
end