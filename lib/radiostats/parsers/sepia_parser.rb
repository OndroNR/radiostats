require 'nokogiri'

module Radiostats
	module Parsers
		class SepiaParser
			def self.parse(xml, instance)
				doc = Nokogiri::XML(xml)
				
				now = doc.css('item1').first
				
				instance.song_name = now.css('skladba').first.content.strip.to_ascii.upcase_word
				instance.interpret = now.css('interpret').first.content.strip.to_ascii.upcase_word
				instance.raw = now.to_xml
				
				instance
			end
		end
	end
end