require 'addressable/uri'

module Radiostats
	module Parsers
		class HeyParser
			def self.parse(data, instance)
				u = Addressable::URI.parse('http://hey.sk/?a=a' + data.to_s)
				
				parts = u.query_values['nazov'].split('-', 2)
												
				instance.song_name = parts[1].strip.to_ascii.upcase_word
				instance.interpret = parts[0].strip.to_ascii.upcase_word
				instance.raw = data
				
				instance
			end
		end
	end
end