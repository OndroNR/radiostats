require 'nokogiri'

module Radiostats
	module Parsers
		class RadiomaxParser
			def self.parse(html, instance)
				doc = Nokogiri::HTML(html)
				
				nbsp = Nokogiri::HTML("&nbsp;").text
				
				raw = doc.css('#now').first.text.gsub(nbsp, " ").strip
				
				parts = raw.split(' - ', 2)
				
				if parts.count != 2
					raise "radiomax parsing failed: count not 2"
				end
				
				instance.song_name = parts[1].strip.to_ascii.upcase_word
				instance.interpret = parts[0].strip.to_ascii.upcase_word
				instance.raw = raw
				
				instance
			end
		end
	end
end