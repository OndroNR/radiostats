module Radiostats
	module Parsers
		class Europa2Parser
			def self.parse(json, instance)
				data = ActiveSupport::JSON.decode(json)
								
				instance.song_name = data["song"].strip.to_ascii.upcase_word
				instance.interpret = data["interpret"].strip.to_ascii.upcase_word
				instance.raw = json
				
				instance
			end
		end
	end
end