require 'nokogiri'

module Radiostats
	module Parsers
		class JemnemelodieParser
			def self.parse(xml, instance)
				doc = Nokogiri::XML(xml)
				
				instance.interpret = doc.css('interpret').first.content.strip.to_ascii.upcase_word
				
				song_name = doc.css('skladba').first.content.strip.to_ascii.upcase_word
				song_name.sub! instance.interpret, ''
				song_name.sub! "** JEMNE MELODIE LIVE **".upcase_word, ""
				
				instance.song_name = song_name.split(' - ', 2).second.strip
				
				instance.raw = xml
				
				instance
			end
		end
	end
end